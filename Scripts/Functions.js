$(document).ajaxStart(function(){
    $('#Loader').html('<IMG alt="Loading..." src="Images/Loading.gif" />');
});
$(document).ajaxStop(function(){
    $('#Loader').html('');
}); 

function ShowHide(Element_id){
    if (document.getElementById(Element_id)){ 
        var Element = document.getElementById(Element_id);
        if (Element.style.display !== "block"){
            Element.style.display = "block";
        }
        else{
            Element.style.display = "none";
        }
    }
}
function RotateArrow(Element_id){
    if (document.getElementById(Element_id)){ 
        var Element = document.getElementById(Element_id);
        if (Element.className === "TriangleDown"){
            Element.className = "TriangleRight";
        }
        else{
            Element.className = "TriangleDown";
        }
    }
}
function ValidateNumberInput(e){
    var Key = e.keyCode || e.charCode || e.which;
    if ((Key < 48 || Key > 57) && 
         Key !== 8 &&
         Key !== 37 &&
         Key !== 39){
        return false;
    }
}
function ResetDB(){
    var Amount = $("#PromtResetDB input").val();
    if (Amount < 1 || Amount > 50000){
        alert("Возможны только знечения в диапазоне от 1 до 50000 включительно!");
    }
    else{
        $.ajax({
            type: 'POST',
            data: {'Action': 'ResetDB', 'Amount': Amount},
            url: 'index.php',
            dataType: 'text'
        });
        ShowHide('PromtResetDB');
        SetView(document.getElementsByClassName('Selected')[0]);
    }
}
function SetView(Sender){
    var Menu = document.getElementById('MenuBar').getElementsByTagName("input");
    for(var i = 0; i < Menu.length; i++){
        Menu[i].removeAttribute('class');
    }
    Sender.setAttribute('class', 'Selected');
    switch(Sender.id){
        case 'Structure':
            $("#Content").load('Views/Structure.html');
            break;
        case 'Table':
            $("#Content").load('Views/Table.html');
            break;
        default:
            $("#Content").load('Views/Structure.html');
            break;
    }
}
function SelectSubordinates(Root){
    if (Root !== null){
        if(Root.getElementsByTagName('div')[0].className === "TriangleDown" &&
            document.getElementById('LevelOf_'+Root.getAttribute('data-employeeid')).childElementCount === 0){
            $.ajax({
                type: 'POST',
                data: {'Action': 'SelectSubordinates', 'ChiefID': Root.getAttribute('data-employeeid')},
                url: 'index.php',
                dataType: 'text',
                success: function(data){
                    if (data.indexOf('<!DOCTYPE html>') !== -1){
                        data = data.replace(data.substr(data.indexOf('<!DOCTYPE html>')), '');
                    }
                    var SubList = '';
                    var Subordinates = new Array();
                    try{
                        Subordinates = JSON.parse(data);
                    }
                    catch(e){}
                    if (Subordinates.length > 0){
                        for (var i = 0; i < Subordinates.length; i++){
                            SubList += '<LI class="StructureElement"'
                            +'" data-employeeid="'+Subordinates[i]["EmployeeID"]
                            +'" onclick="RotateArrow(\'CatalogHider_'+Subordinates[i]["EmployeeID"]
                            +'\'); ShowHide(\'LevelOf_'+Subordinates[i]["EmployeeID"]
                            +'\'); SelectSubordinates(this);">'
                            +'<div id="CatalogHider_'+Subordinates[i]["EmployeeID"]+'" style="float: left;" class="TriangleRight"></div>'
                            +'<SPAN class="NameContainer">'+Subordinates[i]["FullName"]+'</SPAN><BR>'
                            +'<SPAN class="PositionContainer">'+Subordinates[i]["Position"]+'</SPAN></LI>'
                            +'<UL style="display: none;" id="LevelOf_'+Subordinates[i]["EmployeeID"]+'"></UL>';
                        }
                    }
                    else{
                        SubList = '<SPAN class="PositionContainer" style="color: black;">Нет подчинённых</SPAN>';
                    }
                    $('#LevelOf_'+Root.getAttribute('data-employeeid')).html(SubList);
                }
            });
        }
    }
    else{
        $.ajax({
            type: 'POST',
            data: {'Action': 'SelectSubordinates', 'ChiefID': 0},
            url: 'index.php',
            dataType: 'text',
            success: function(data){
                if (data.indexOf('<!DOCTYPE html>') !== -1){
                    data = data.replace(data.substr(data.indexOf('<!DOCTYPE html>')), '');
                }
                var SubList = '';
                var Subordinates = new Array();
                try{
                    Subordinates = JSON.parse(data);
                }
                catch(e){}
                
                if (Subordinates.length > 0){
                    for (var i = 0; i < Subordinates.length; i++){
                        SubList += '<LI class="StructureElement"'
                        +'" data-employeeid="'+Subordinates[i]["EmployeeID"]
                        +'" onclick="RotateArrow(\'CatalogHider_'+Subordinates[i]["EmployeeID"]
                        +'\'); ShowHide(\'LevelOf_'+Subordinates[i]["EmployeeID"]
                        +'\'); SelectSubordinates(this);">'
                        +'<div id="CatalogHider_'+Subordinates[i]["EmployeeID"]+'" style="float: left;" class="TriangleRight"></div>'
                        +'<SPAN class="NameContainer">'+Subordinates[i]["FullName"]+'</SPAN><BR>'
                        +'<SPAN class="PositionContainer">'+Subordinates[i]["Position"]+'</SPAN></LI>'
                        +'<UL style="display: none;" id="LevelOf_'+Subordinates[i]["EmployeeID"]+'"></UL>';
                    }
                }
                else{
                    SubList = '<SPAN class="PositionContainer" style="color: black;">Нет подчинений</SPAN>';
                }
                $('#StructureView ul').html(SubList);
            }
        });
    }
}
function BuildTable(Filter){
    $.ajax({
        type: 'POST',
        data: {'Action': 'LoadTableData',
            'Limit': $('#SelectedAmount').val(),
            'Offset': 0,
            'Sort': GetSort(),
            'Filter': Filter},
        url: 'index.php',
        dataType: 'text',
        success: function(data){
            if (data.indexOf('<!DOCTYPE html>') !== -1){
                data = data.replace(data.substr(data.indexOf('<!DOCTYPE html>')), '');
            }
            var SubList = '';
            var Employees = new Array();
            try{
                Employees = JSON.parse(data);
            }
            catch(e){}
            
            if(Employees.length > 0){
                for(var i = 0; i < Employees.length; i++){
                    SubList += '<DIV class="TableList" style="display: block;">'
                            +'<DIV style="width: calc(5% - 9px); text-align: right;">'+Employees[i]['EmployeeID']+'</DIV>'
                            +'<DIV style="width: calc(25% - 9px);">'+Employees[i]['FullName']+'</DIV>'
                            +'<DIV style="width: calc(25% - 9px);">'+Employees[i]['Position']+'</DIV>'
                            +'<DIV style="width: calc(10% - 9px); text-align: center;">'+Employees[i]['HiringDate']+'</DIV>'
                            +'<DIV style="width: calc(10% - 9px); text-align: right;">'+Employees[i]['Salary']+'</DIV>'
                            +'<DIV style="width: calc(25% - 18px);">'+Employees[i]['Chief']+'</DIV></DIV>';
                }   
            }
            else{
//                alert(data);
                SubList = '<DIV>Записи отсутствуют</DIV>'; 
            }
            $('#EmployeesGrid').html(SubList);
        }
    });
}
function GetRecordsCount(){
    $.ajax({
        type: 'POST',
        data: {'Action': 'GetRecordsCount'},
        url: 'index.php',
        dataType: 'text',
        success: function(data){
            if (data.indexOf('<!DOCTYPE html>') !== -1){
                data = data.replace(data.substr(data.indexOf('<!DOCTYPE html>')), '');
            }
            $('#AllRecords').val(data);
            $('#RecordsCount').html(data);
            $('#InfoBlock').html('&nbsp;на странице&nbsp;/&nbsp;всего&nbsp;');
            $('#SelectedAmount').css('display','block');
        }
    });
}
function GetSort(Object){
    var Filter = document.getElementsByClassName('SortUpMarkerSelected')[0];
    if(Filter === undefined){
        Filter = document.getElementsByClassName('SortDownMarkerSelected')[0];
    }
    if (Object){
        return Filter;
    }
    else{
        return Filter.getAttribute('data-sort');
    }    
}
function SetSort(SelectedFilter){
    var CurrentFilter = GetSort(true);
    if (CurrentFilter.className === 'SortUpMarkerSelected'){
        CurrentFilter.className = 'SortUpMarker';
    }
    else{
        CurrentFilter.className = 'SortDownMarker';
    }
    if (SelectedFilter.className === 'SortUpMarker'){
        SelectedFilter.className = 'SortUpMarkerSelected';
    }
    else{
        SelectedFilter.className = 'SortDownMarkerSelected';
    }
    FiltersConsolidation();
}
function DateInputFilter(Input){
    if (Input.value.length > 10){
        Input.value = Input.value.slice(0,10);
    }
    else if (Input.value.length === 4 && Input.value.indexOf("-") === -1){
        Input.value += "-";
    }
    else if (Input.value.length === 7 && Input.value.substr(5, 2).indexOf("-") === -1){
        Input.value += "-";
    }
    else if (Input.value.length < 4){
        Input.value.replace("-", "");
    }
}
function EnterDetect(e){
    var Key = e.keyCode || e.charCode || e.which;
    if (Key === 13){
         FiltersConsolidation();
    }
}
function ResetAllFilters(){
    var FiltersBlock = document.getElementsByClassName('TableListHeader')[0];
    var Handlers = FiltersBlock.getElementsByTagName('a');
    var Filters = FiltersBlock.getElementsByTagName('input');
    for(var i = 0; i < Handlers.length; i++){
        Handlers[i].className = 'Filter';
        Filters[i].value = '';
    }
}
function SelectFilter(Input){
    var FiltersBlock = document.getElementsByClassName('TableListHeader')[0];
    var Handlers = FiltersBlock.getElementsByTagName('a');
    var Filters = FiltersBlock.getElementsByTagName('input');
    var MainHandler = document.getElementById('FilterReset');
    var index = 0;
    for(var i = 0; i < Filters.length; i++){
        if(Filters[i].getAttribute('data-filter') === Input.getAttribute('data-filter')){
            index = i;
            break;
        }
    }
    if (Input.value.length > 0){
        Handlers[index].className = "FilterSelected";
        MainHandler.className = 'FilterIconActive';
    }
    else{
        Handlers[index].className = "Filter";
        var AllClear = true;
        for(var i = 0; i < Filters.length; i++){
            if(Filters[i].value.length > 0){
                AllClear = false;
                break;
            }
        }
        if (AllClear){
            MainHandler.className = 'FilterIcon';
        }
    }
}
function FiltersConsolidation(){
    var FiltersBlock = document.getElementsByClassName('TableListHeader')[0];
    var Filters = FiltersBlock.getElementsByTagName('input');
    var ActiveFilters = new Array();
    var QueryFilter = '';
    for(var i = 0; i < Filters.length; i++){
        if(Filters[i].value.length > 0){
            ActiveFilters.push(Filters[i]);
        }
    }
    var Operator = 'WHERE';
    for(var i = 0; i < ActiveFilters.length; i++){
        if (i > 0){
            Operator = 'AND';
        }
        switch(ActiveFilters[i].getAttribute('data-filter')){
            case 'ID':
                QueryFilter += ' '+Operator+' A.EmployeeID = \'' + ActiveFilters[i].value + '\'';
                break;
            case 'FullName':
                QueryFilter += ' '+Operator+' A.FullName LIKE \'%' + ActiveFilters[i].value + '%\'';
                break;
            case 'Position':
                QueryFilter += ' '+Operator+' A.Position LIKE \'%' + ActiveFilters[i].value + '%\'';
                break;
            case 'HiringDate':
                QueryFilter += ' '+Operator+' A.HiringDate LIKE \'%' + ActiveFilters[i].value + '%\'';
                break;
            case 'Salary':
                QueryFilter += ' '+Operator+' A.Salary >= \'' + ActiveFilters[i].value + '\'';
                break;
            case 'Chief':
                QueryFilter += ' '+Operator+' A.ChiefID = (SELECT EmployeeID FROM Employees WHERE FullName LIKE\'%' + ActiveFilters[i].value + '%\')';
                break;
            default:
                break;
        }
    }
    BuildTable(QueryFilter);
}