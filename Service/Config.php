<?php defined('ValidCall') or die('Access denied! Please use the application main page');

define("HOST", "localhost");
define("USER", "root");
define("PASSWORD", "");
define("DATABASE", "ABZ_TestTask_DB");

$Connection = mysqli_connect(HOST, USER, PASSWORD) or die(mysql_error());

if(!mysqli_select_db($Connection, DATABASE)){
    mysqli_query($Connection, "CREATE DATABASE ".DATABASE);
}
$Connection = mysqli_connect(HOST, USER, PASSWORD, DATABASE);
// mysqli_query($Connection, "SET SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO'; SET time_zone = '+00:00'");

$QueryText = "CREATE TABLE IF NOT EXISTS `Employees` (
    `EmployeeID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID сотрудника',
    `FullName` varchar(100) NOT NULL COMMENT 'Имя сотрудника',
    `Position` varchar(100) NOT NULL COMMENT 'Должность сотрудника',
    `HiringDate` date NOT NULL COMMENT 'Дата приёма на работу',
    `Salary` double(8,2) NOT NULL COMMENT 'Оклад',
    `ChiefID` int(11) DEFAULT NULL COMMENT 'ID руководителя',
    PRIMARY KEY (EmployeeID)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

mysqli_query($Connection, $QueryText); 