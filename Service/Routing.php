<?php defined('ValidCall') or die('Access denied! Please use the application main page');

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $Data = new stdClass();
            foreach ($_POST as $Key=>$Val){
                $Data->$Key = $Val;
            }
    switch($Data->Action){
        case 'ResetDB':
            echo DatabaseRandomFill($Connection, $Data->Amount);
            break;
        case 'SelectSubordinates':
            echo SelectSubordinates($Connection, $Data->ChiefID);
            break;
        case 'LoadTableData':
            echo GatherEmployeesData($Connection, $Data->Limit, $Data->Offset, $Data->Sort, $Data->Filter);
            break;
        case 'GetRecordsCount':
            echo GetRecordsCount($Connection);
            break;
        default:
            break;
    }
}