<?php defined('ValidCall') or die('Access denied! Please use the application main page');
//FILL DATABASE
function DatabaseRandomFill($Connection, $RecordsCount){
    $QueryText = "TRUNCATE TABLE Employees";
    mysqli_query($Connection, $QueryText) or die("Ошибка соединения с базой данных");
    $BaseStringLength = 2;
    $BaseSalary = 100;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    $QueryText = "INSERT INTO Employees (FullName, Position, HiringDate, Salary, ChiefID) VALUES";
    for($i = 0; $i < $RecordsCount; $i++){
        $FullName = GenerateRandomString($BaseStringLength, $BaseStringLength * 6).
                " ".GenerateRandomString($BaseStringLength, $BaseStringLength * 6).
                " ".GenerateRandomString($BaseStringLength, $BaseStringLength * 6);
        $HiringDate = GenerateRamdomDate('2000-01-01', '2016-09-01');
        
        if ($i > 0){
            $Position = GenerateRandomString($BaseStringLength * 3, $BaseStringLength * 25, " ");
            $Salary = GenerateRandomDouble($BaseSalary, $BaseSalary * 100);
            $ChiefID = mt_rand(1, $i);
        }
        else{
            $Position = "Chief Executive Officer";
            $Salary = $BaseSalary * 100;
            $ChiefID = 0;
        }
        if ($i != $RecordsCount - 1){
            $QueryText.= " ('$FullName', '$Position', '$HiringDate', '$Salary', '$ChiefID'),";
        }
        else{
            $QueryText.= " ('$FullName', '$Position', '$HiringDate', '$Salary', '$ChiefID')";
        }
    }
    mysqli_query($Connection, $QueryText) or die("Ошибка соединения с базой данных");
    return "База данных перезагужена. Количество записей: ".$RecordsCount;
}
//DATA WORKFLOWS
function GetRecordsCount($Connection){
    $QueryText = 'SELECT COUNT(EmployeeID) AS RecordsCount FROM Employees';
    $RecordsCountQuery = mysqli_query($Connection, $QueryText) or die("Ошибка соединения с базой данных");
    if ($RecordsCountQuery != null){
        while ($Response = mysqli_fetch_array($RecordsCountQuery, MYSQLI_ASSOC)){
            $RecordsCount = $Response;
        }
    }
    return $RecordsCount['RecordsCount'];
}
function GatherEmployeesData($Connection, $Limit, $Offset, $SortType, $Filter){
    $Employees = Array();
    if ($Filter == ''){
        $SupportFilter = " WHERE ChiefID = '0' ";
    }
    else{
        $SupportFilter = str_replace('A.', '', $Filter)." AND ChiefID = '0'";
    }
    $QueryText = "SELECT *, 'N/A' AS Chief FROM Employees".$SupportFilter
    ." UNION SELECT A.*, B.FullName AS Chief FROM Employees A
    INNER JOIN Employees B ON A.ChiefID = B.EmployeeID".$Filter.
    " ORDER BY ".$SortType." LIMIT ".$Limit." OFFSET ".$Offset;
    $EmployeesQuery = mysqli_query($Connection, $QueryText);
    if ($EmployeesQuery != null){
        while ($Response = mysqli_fetch_array($EmployeesQuery, MYSQLI_ASSOC)){
            $Employees[] = $Response;
        }
    }
    return json_encode($Employees);
//    return $QueryText;
}
function SelectSubordinates($Connection, $ChiefID){
    $Subordinates = Array();
    $QueryText = "SELECT * FROM Employees WHERE ChiefID='$ChiefID'";
    $SubordinatesQuery = mysqli_query($Connection, $QueryText);
    if (SubordinatesQuery != null){
        while ($Response = mysqli_fetch_array($SubordinatesQuery, MYSQLI_ASSOC)){
            $Subordinates[] = $Response;
        }
    }
    return json_encode($Subordinates);
}
//RANDOM GENERATORS
function GenerateRandomString($MinLength, $MaxLength, $ExtraChars = ""){
    if ($MinLength > $MaxLength){
        $MinLength = $MaxLength;
    }
    
    $AvailableChars = "abcdefghijklmnopqrstuvwxyz".$ExtraChars;
    $StringLength = rand($MinLength, $MaxLength);
    for ($i = 0; $i < $StringLength; $i++){
        if ($i > 0){
            $Result.= $AvailableChars[mt_rand(0, strlen($AvailableChars) - 1)];
        }
        else{
            $Result.= strtoupper($AvailableChars[mt_rand(0, strlen($AvailableChars) - 1)]);
        }
    }
    return $Result;
}
function GenerateRandomDouble($MinValue, $MaxValue){
    if ($MinValue > $MaxValue){
        $MinValue = $MaxValue;
    }
    $Result = mt_rand($MinValue * 100, $MaxValue * 100) / 100;
    return round($Result, 2);
}
function GenerateRamdomDate($MinDate, $MaxDate){
    $MinDate = strtotime($MinDate);
    $MaxDate = strtotime($MaxDate);
    if ($MinDate > $MaxDate){
        $MinDate = $MaxDate;
    }
    $Result = mt_rand($MinDate, $MaxDate);
    return date("Y-m-d", $Result);
}