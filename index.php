<?php
define('ValidCall', true);
if (session_status() == PHP_SESSION_NONE){
    session_start();
}
require_once 'Service/Config.php';
require_once 'Service/Functions.php';
require_once 'Service/Routing.php';
?>
<!DOCTYPE html>
<HTML>
<HEAD>
    <meta charset="utf-8">
    <TITLE></TITLE>
    <LINK rel="stylesheet" href="Styles/Global.css">
    <LINK rel="stylesheet" href="Styles/Overlays.css">
    <LINK rel="stylesheet" href="Styles/Tables.css">
    <script type="text/javascript" src="Scripts/jQuery.js"></script>
    <script type="text/javascript" src="Scripts/Functions.js"></script>
</HEAD>
<BODY>
    <!--<noscript><DIV class="GlobalOverlay"><DIV>Необходимо включить JavaScript</DIV></DIV></noscript>-->
    <DIV class="Page">
        <DIV class="Overlay" id="PromtResetDB" style="display: none;">
            <DIV class="MainFrame" style="width: 200px;">
                <SPAN style="display: block; float: left; width: 150px; height: 24px; padding: 3px 0;">Количество записей:</SPAN>
                <INPUT class="Input" id="RecordsAmount" style="width: 38px;" type="text" value="1000" onkeypress="return ValidateNumberInput(event);">
                <INPUT class="Button" style="float: left; width: 90px; margin-left: 0;" type="button" value="ПРИНЯТЬ" onclick="ResetDB({'Action': 'ResetDB', 'Amount': $('#RecordsAmount').val()}); ">
                <INPUT class="Button" style="float: left; width: 90px; margin-right: 0;" type="button" value="ОТМЕНА" onclick="ShowHide('PromtResetDB');">
                <DIV class="BreakFloat"></DIV>
            </DIV>
        </DIV>
        <DIV id="Header">
            <UL id="MenuBar">
                <LI><INPUT class="Selected" type="button" value="Структура" id="Structure" onclick="SetView(this);"></LI>
                <LI><INPUT type="button" value="Таблица" id="Table" onclick="SetView(this);"></LI>
                <LI><INPUT style="color: darkred; text-shadow: azure 0 0 1px;" type="button" value="Перезагрузка БД" onclick="ShowHide('PromtResetDB');"></LI>
            </UL>
        </DIV>
        <DIV id="Loader"></DIV>
        <DIV id="Content"> 
            <SCRIPT>SetView(document.getElementsByClassName('Selected')[0]);</SCRIPT>
        </DIV>
        <DIV id="Footer"></DIV>
    </DIV>
</BODY>
</HTML>